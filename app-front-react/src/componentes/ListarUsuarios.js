//Importamos el REACT.COMPONENT
import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import App from './App';
import EditUserForm from './EditUserForm';


class ListarUsuario extends /* React.*/ React.Component{  //Podriamos no poner React.Componente

    constructor(props){
        super(props);
        

                this.onClickBorrar = this.onClickBorrar.bind(this);
                this.EditUserForm = this.EditUserForm.bind(this);

    }
 

    componentDidMount(){
        
        let promesaHTTP = window.fetch('http://127.0.0.1:4000/api/usuarios');
        
        promesaHTTP.then( (resHttp) => {

        let promesaJSON= resHttp.json();

             promesaJSON.then( (objColeccionUsu) => {
                    console.log(JSON.stringify(objColeccionUsu));
                    //Cuando por fin recibimos la coleccion y ha sido convertida en JSON, 
                 this.setState({
                     ListarUsuario: objColeccionUsu
                 });
 
             });
         });
         
            this.obtenerUsu();


    }
    //TODO:Creado avento para borrar de la bbdd un usuario en concreto, Recogemos el ID del usuario,/**************FALTA MENSAJE DE EXITO O FRACASO***********
    onClickBorrar(evt){
        let el = evt.target;
        let id = el.dataset.idusu;
        
       
        console.log(`Borrar ID seleccionado: ${id}`);
        window.fetch(`http://localhost:4000/api/usuarios/${id}`, {
            method: 'DELETE',
            mode: 'cors'
        })  
      
        .catch(err => console.error('No se ha borrado..... :('))
        .then(res => alert('Borrado'));
        window.location.reload();


    }

    async  obtenerUsu(){
        await window.fetch('http://127.0.0.1:4000/api/usuarios')
            .then(res => res.json())
            .then(obj => this.setState({listaUsuarios: obj}));
    }

    async  EditUserForm(evt, id) {
        evt.preventDefault();
        console.log(id);
       
        await window.fetch(`http://127.0.0.1:4000/api/usuarios/editar/${id}`, {
            method: 'PUT',
            body: JSON.stringify({

                "nombre": this.state.nombre,
                "edad":this.state.edad,
                "email": this.state.email,
                "password": this.state.password,
            }),
            
            headers: {
                'Content-type': 'application/json'
            }
        });
    }
    componentWillMount(){

    }
   
    
    render(){   
    //TODO:  Condicional, si this.state no existe, mostramos "cargando......"
    let objViDomJSX;
            if(this.state === null){
                 objViDomJSX = (<p>cargando......</p>)
           

                //   let filasTr = this.state.ListarUsuario.map((usu) =>{

                //     return(                        
                //     <tr key={ contIds }>

                //         <td>
                //         {usu.nombre}
                //         </td>

                //         <td>
                //         {usu.email}
                //         </td>

                //         <td>
                //         {usu.password}
                //         </td>

                //     </tr>
                //     );

                //   });


            }else{
                 objViDomJSX = (
             <div className="container">
                 <div className="wrapper">
                     <div className="row">
                         <div className="col-xs-6">
                             
<div>
                <h1 className="cabeceraTabla">Lista de Usuarios</h1>
                <table className="table table-dark table-condensed ">
                    <thead> 
                        <tr>
                            <th>
                            <span className="custom-checkbox">
                            <input type="checkbox" id="selectAll"></input>
                            <label htmlFor="selectAll"></label>
                            </span>

                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Mail
                            </th>
                            <th>
                                Edad
                            </th>
                            <th>
                                Password
                            </th>
                            <th>
                                Accion
                            </th>
                        </tr>
                        
                    </thead>

                    <tbody>

                        { this.state.ListarUsuario.map( usu => (
                                
                                                      
                                <tr key={ usu._id }>

                                    <td>
                                    <span className="custom-checkbox">
                                    <input type="checkbox" id="selectAll"></input>
                                    <label htmlFor="selectAll"></label>
							        </span>

                                    </td>

                                    <td>
                                    {usu.nombre}
                                    </td>

                                    <td>
                                    {usu.email}
                                    </td>

                                    <td>
                                    {usu.edad}
                                    </td>

                                    <td>
                                    {usu.password}
                                    </td>

                                    <td>
                                    

                                    {/* Nos muestra el icono de editar y borrar pero no sabemos darle la funcionalidad */}
                                    {/* <a href="#editEmployeeModal" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Editar">&#xE254;</i></a>
                                    <a href="#deleteEmployeeModal" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a> */}


                                    <Link to={`/editar/${usu._id}`} value="Edit">Editar</Link>    
                                    <input type="button" data-idusu={usu._id} onClick={this.onClickBorrar} value="Delete"></input>

                                   </td>
                                 
                                </tr>

            

                                )
                        ) 
                    }

                    
            
                    </tbody>
                </table>

            </div>

                 </div>
               </div>
               </div>
              
             </div>
             
             
                    
                 );
                 /* *********************Editar */
              
            }
          
        
            
           
        return (objViDomJSX);
            
     }    
     
    
}

export default ListarUsuario;  //Exportamos esta clase para importarla en APP.js