//Importamos el REACT.COMPONENT
import React, { Component } from 'react';
import App from './ListarUsuarios';


class CreaUsuario extends /* React.*/ React.Component{  //Podriamos no poner React.Componente

    /*This.props es objeto con datos pulicos
    this.state objeto con datos privados, es decir,
    el estado interno del componente. Como en Angular
    las variables miembro de la clase privadas */

    constructor(props){
        super(props);
            //VALIDACIONES*******
 
            
        
    
        
        /*
        Para evitar el problema del this con JS, hacemos lo siguiente
        con bind() hacemos que el futuro cuando se invoca el metodo , 
        this sea realmente this, es decir, el objeto instanciado 
        basado en clase, en estado, cada uno de los componentes
        */
        this.onChangeNombre = this.onChangeNombre.bind(this);
        this.onChangeMail = this.onChangeMail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeEdad = this.onChangeEdad.bind(this);
        this.onSubmit = this.onSubmit.bind(this);


        this.state= {
            nombre: 'Flow',
            email: 'email@gmail.com',
            edad: '31',
            password: '1234'
            
        }
    }

    onSubmit(evt){           

            // if(this.state.nombre === null || this.state.nombre === undefined || this.state.nombre.length < 1){
            //     alert('Introduce un Nombre Valido');

            // } else {
            //     if(this.state.edad === null || this.state.edad === undefined ){
            //         alert('Introduce una Edad Valida');
            //     } else {
            //         this.state.edad = parseInt(this.state.edadString);
            //         if (this.state.edad < 18) {                    
            //             alert('Introduce una Edad  > 18');
            //         } else {

            //         }
            //     }
            // }else if(this.state.email === null || this.state.email === undefined || this.state.email.length < 15){
            //     alert('Introduce un Email Valido');

            // }else if(this.state.password === null || this.state.password === undefined || this.state.password.length < 8){
            //     alert('Introduce una Contraseña Valida');

            // }else

            // {
            evt.preventDefault();
            console.log( `Datos: ${this.state.nombre}, ${this.state.email}, ${this.state.password},`);
            window.fetch('http://127.0.0.1:4000/api/usuarios/registro', {
            method: 'POST',
            body: JSON.stringify({

                    "_id": this.state._id,
                    "nombre": this.state.nombre,
                    "email": this.state.email,
                    "edad": this.state.edad,
                    "password": this.state.password
                    
            }),
            headers:{
                'Content-Type': "application/json"
            }
            

            }).then((res) => alert('Datos introducidos con EXITO!!!!!'))
            .catch((err) => alert('No se ha guardado los datos... :('));
        


                // alert('OK');
            }
   
        
    



    /*Metodo es invocado por react, cada vez que se cambia el valor del <input> 
    se envia un objeto con la informacion del evento */
    onChangeMail(evt){

        this.setState({
                email: evt.target.value
        });
    }

    onChangePassword(evt){

        this.setState({
                password: evt.target.value
        });
    }

    onChangeNombre(evt){
    //  if(this.state.nombre === null || this.state.nombre === undefined || this.state.nombre.length < 1){
    //         this.state.mensaje = "Introduce-..";

    //  }else{
        this.setState({

            nombre: evt.target.value,
            // usuario: 
            //     { 
            //     nombre: 'Flow',
            //     email: 'email@gmail.com',
            //     edad: '31',
            //     password: '1234',     
            //     },
            // mensaje: 'Introduce un nombre Valido'
        })
      } 
    //}      
  
 

    
    onChangeEdad(evt){

        this.setState({
            edadString: evt.target.value
        });
    }

    render(){
        
        return(
        
                <div>
                <h1 className="cabeceraTabla">Formulario para Crear Usuarios</h1>

                <form onSubmit= { this.onSubmit }>
                    
                    <div className="modal-body">
                    {this.state.mensaje}
                                    <div className="form-group">
                                        <label>Usuario: </label>
                                        <input type= "text"
                                                placeholder= "FLow"
                                                value = {this.state.nombre}
                                                onChange = {this.onChangeNombre}
                                                className="form-control" required />

                                    </div>

                                    <div className="form-group">
                                        <label>Edad: </label>
                                        <input  type="number"
                                                placeholder= "Edad"
                                                value = {this.state.edad}
                                                onChange = {this.onChangeEdad}
                                                className="form-control" required />

                                    </div>

                                    <div className="form-group">
                                        <label>Email: </label>
                                        <input type= "email"
                                                placeholder= "ejemplo@gmail.com"
                                                value = {this.state.email}
                                                onChange = {this.onChangeMail}
                                                className="form-control" required />

                                    </div>

                                    <div className="form-group">
                                        <label>Contraseña: </label>
                                        <input type= "password"
                                            placeholder= "CoNtRaSeÑa"
                                            value = {this.state.password}
                                            onChange = {this.onChangePassword}
                                            className="form-control" required />
                                    </div>

                                <div className="modal-footer">

                            <input type="submit" value="Registrar" className="btn btn-success"/>
                        </div>
                    </div>
                </form>

                
               </div>
                   

        );
    }
}

export default CreaUsuario; //Exportamos esta clase para importarla en APP.js
