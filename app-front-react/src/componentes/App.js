//Crearemos aqui el componente RAIZ del proyecto
//El "react" viene del node_modules

import React from "react";  //Importamos mod React
import { BrowserRouter as Router, Route, Link} from 'react-router-dom';
import logo from "../logo.svg"; //Importamos el fichero
import "./App.css";
import CrearUsuario from "./CrearUsuario";
import ListarUsuario from "./ListarUsuarios";
import EditUserForm from "./EditUserForm";



function App(){

    let estiloLogo = {  //Objeto JavaScript con propiedades CSS

        width: "10em",
        heigth: "10em"
    }

    return(


        <Router>

            <div className="">
            
                <header className="App-header">
                    <img src= { logo } style = {estiloLogo}
                    className="App-logo" alt= "logo"></img>
                    
                    <p>Operaciones CRUD usuario</p>
                </header>

                <nav className="menuPrincipal">
                <Link to= "/">Lista de Usuarios</Link> - - - - <Link to= "/registro">Crear Nuevo Usuario</Link> 
                </nav>

            {/*Aqui tenemos las rutas para redirigir a las paginas*/}
            <Route path= "/" exact component= {ListarUsuario}/>
            <Route path= "/registro" component= {CrearUsuario}/>
            <Route path= "/editar" component= {EditUserForm}/>

           
            </div>
             
        </Router>
    );
}

export default App;

