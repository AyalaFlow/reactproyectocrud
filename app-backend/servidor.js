const express = require('express');
const bodyParser = require('body-parser');

//TODO: Importar y usar el modulo middle-ware CORS
const mongoose = require('mongoose');
const Usuario = require('./modelo');
const cors = require('cors');

const app = express(); //Declaramos una constante para la funcion de express
const PORT = 4000;  //Declaramos el puerto con una constante.

/*Sofware intermediario para la serializacion y deserializacion (parseo) automática. */
app.use(bodyParser.json());
app.use(cors());


//127.0.0.1: es lo mismo que Localhost:
mongoose.connect('mongodb://127.0.0.1:27017/db_usuarios')//cadena de conexion a la bbdd, [[[IMPORTANTISIMO, PARA CADA TIPO DE BBDD, TIENE SU PROPIA CADENA DE CONEXION]]]
const conexion =mongoose.connection;
 //once() es lo mismo que addEventListener
conexion.once("open",function(){
      console.log(' 0)-¡Yehaaaa Conectados con MongoDB!');

});

/*Metodo para levantar el servidor con la BBDD primero el puerto y luego que nos muestre en consola que se ha levantado*/
app.listen(PORT, 
    function(){
        console.log('1)-Servidor Levantado!!!! ' + " Puerto: " + PORT);
    });

    //Este objeto hace de intermediario, en url /api/usuarios
const rutasApi = express.Router();

app.use("/api/usuarios", rutasApi);

//http://127.0.0.1:4000/api/usuarios/registro metodo POST
function recibirRegistroPost(peticionHttp, respuestaHttp){
    console.log(' 2)-La peticion HTTP empieza a ser procesada')
    //Deberiamos recibir un Json con el nuevo usuario
    //Asi que creamos un objeto Schemay le pasamos el Json ya
    //convertido en Objeto de JS, gracias al body-parser

    let nuevoUsuario = new Usuario(peticionHttp.body);
    let promesaDeGuardado = nuevoUsuario.save();
    promesaDeGuardado.then( usuario =>{
        console.log(' 4)-Se ha guardado en BBDD MongoDB');
    respuestaHttp.status(200).json({
            "usuario": "Creado satisfactoriamente"
        })
    });
    promesaDeGuardado.catch( error =>{
        console.log(' 4)-El registro ha fallado')
        respuestaHttp.status(400).send("El registro ha fallado");

    } );
        console.log(" 3)-La peticion HTTP ha sido procesada");
}





rutasApi.route("/registro").post(recibirRegistroPost);
rutasApi.route("/editar/:id").post(recibirRegistroPost);

rutasApi.route('/').get(function(reqPeticionHttp, resRespuestaHttp){

//Pide toda la coleccion e invoca a esta callback con ella y el error
//Invoca a la query de Mongo db.usuarios.find()
    Usuario.find(function(err, coleccionUsuarios){
        if(err){
            console.log(err);
        }else{
            //Pedimos devolver la collection en formato JSON
            resRespuestaHttp.json(coleccionUsuarios);
        }

    });
});




rutasApi.route('/:id').delete(function(reqHttp, resHttp){
    let id = reqHttp.params.id;
    console.log('Eliminando' + id)
    let consultaFindOne = Usuario.findById({_id: id});

    consultaFindOne.exec((err, resDoc) => {
     
        if(err){
            resHttp.json({"mensaje": "Error al buscar un usuario para eliminar, " + err});
        }else{
            if(resDoc == null){
                resHttp.json({"mensaje": "No se ha encontrado el usuario"});
            }else{   //Si no hay error y resDoc es distinto de null
                console.log('Se ha encontrado, ahora eliminar' + resDoc);


                consultaFindOne.deleteOne().exec((err, resDoc2)=>
                {
                    let msjResp = "";
                    if(resDoc2.deletedCount >= 1){
                        msjResp = 'Usuario ELIMINADO!!!!';
                    }else
                        {
                        msjResp = 'Usuario NO eliminado, ¡Sigue existiendo!' + resDoc;
                        }
    
                    
                    console.log(resDoc2);
                    resHttp.json({"mensaje": msjResp});
                });
            }
        
        }
    })
});    
//     Usuario.findById(req.params.id,(err, res) =>{
//         console.log("Antes de eliminar:  " + err);
//         console.log("Antes de eliminar:  " + res);
//         if (err == null && res ==null) {
//             let error =  {
//                 resultado : "No existe.. :("
//             }
//             .json(error);
//         } else if (res != null) {
//             Usuario.findById(req.params.id).remove().exec();

//             Usuario.findById(req.params.id,(err, res) =>{
//                 if (err == null && res ==null) {

//                     console.log(`usuario ${req.params.id} borrado satisfactoriamente`);
//                     let respuesta = {     

//                         resultado : 'Usuario borrado con Exito!!!'   
//                     }
//                     .json(respuesta);

//                 } else {
//                     let error =  {
//                         resultado : "No se ha borrado :("
//                     }
//                     .json(error);
//                 }
//             });
//         }            
//     }) ;    
// });



rutasApi.route('/editar/:id').put(async function(req, res){
    let id = req.params.id;
    //Creamos una constante con los campos que queremos 
    const identidad = {
        id: req.params.id,
        nombre: req.body.nombre, //Requerimos el cuerpo del nombre y asi con los demas campos.
        edad: req.body.edad,
        email: req.body.email,
        password: req.body.password
    };

    await Usuario.findByIdAndUpdate(id, {$set: identidad});
    console.log(`Usuario ${id} editado satisfactoriamente`);

    res.json({
        status: 'Empleado modificado'
    });

});
    